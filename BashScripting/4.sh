#!bin/bash

directory=$1
output=/home/emumba/backup

if [ ! -d $directory ]; then
        echo "Requested $directory user home directory doesn't exist."
        exit 1                                               
fi 
if [ ! -d $output ]; then
        mkdir $output                                              
fi 


cd $directory
for file1 in *.c; do
    if  [ -f $output/$file1 ];then 
        if diff $directory/$file1 $output/$file1 -q; then
            echo $file1 already backedup
        else
            cp $directory/$file1 $output/$file1 -v
        fi
    else 
        cp $directory/$file1 $output/$file1 -v
    fi
done
