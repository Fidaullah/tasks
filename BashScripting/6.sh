#!bin/bash

awk '{print $4}' raw_data_for_stats_calculation.txt  > number_list.txt
sort -n -o sorted_number_list.txt number_list.txt
n=200
median=$(($n / 2))
minimum=1
maximum=$n
sum=0
#sum
while read i; do
    p=$(echo $i | grep -o -E '[0-9]+')
    sum=$(($sum + $p))
done < sorted_number_list.txt

minimum=$( awk   'NR==1 {print}' sorted_number_list.txt | grep -o -E '[0-9]+' )
median1=$( awk -v r="$median" 'NR==r {print}' sorted_number_list.txt | grep -o -E '[0-9]+' )
median2=$( awk -v r="$(($median + 1))" 'NR==r {print}' sorted_number_list.txt | grep -o -E '[0-9]+' )


if [ $(($n % 2)) -eq 0 ]; then 
    median=$(( $(($median1 + $median2)) /  2 ))
else
    median=$median1
fi

echo "sum     = $sum"
echo "average = $(($sum / n))"
echo "maximum = $p"
echo "minimum = $minimum"
echo "median  = $median"
